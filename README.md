# autodevops-templates

Here lies the standard gitlab-ci pipeline templates. There serve a sensible default
for new projects. The idea is to have one template per project type that can be
maintained re-used across many projects. These template whould encourage a standard
set of build steps and technologies

The inspiration for this repo came from 
https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Auto-DevOps.gitlab-ci.yml

You can read more about the auto devops concept at:
https://docs.gitlab.com/ee/topics/autodevops/

## Templates
gatsby-gitlab-ci.yml - GatsbyJs projects

## How to use
Create .gitlab-ci.yml file in the root of you project
Include reference to template and set the require variables e.g.

```
include:
  - 'https://lwb.githost.io/open-source/autodevops-templates/raw/master/gatsby.gitlab-ci.yml'

variables:
  MY_REQUIRED_VARIABLE: abcdef
```
